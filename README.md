# DynamicWallpaper-MVVM

 
[APK下载](http://theone.0851zy.com/2021/12/16/b7a2d586408378a380b9440694cfed4b.apk)
 
[演示视频](http://theone.0851zy.com/2021/12/16/e5c44ef24096cd1c80451bb2fa37d789.mp4)
 

#### 动态壁纸

- 使用本地视频设置为壁纸。
- 抖音、快手解析无水印视频，下载功能。
- Kotlin+MVVM。
 
#### 无水印解析
 
 1. 【抖音、快手】分享 -> 复制链接 -> 打开此App -> 获取剪切板内容进行解析.
 2. 【快手】分享 -> 更多 -> 选择**动态壁纸（此App)**

#### 感谢

[Android 仿火萤视频桌面 神奇的LiveWallPaper](https://blog.csdn.net/lmj623565791/article/details/72170299)
 
[java实现抖音、快手短视频解析去除水印下载无水印视频](https://www.it610.com/article/1293444797025034240.htm)
